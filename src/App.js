import logo from "./logo.svg";
import "./App.css";
import Navbar from "./BaiTapThucHanhLayOut/Navbar";
import Header from "./BaiTapThucHanhLayOut/Header";
import Item from "./BaiTapThucHanhLayOut/Item";
import Footer from "./BaiTapThucHanhLayOut/Footer";

function App() {
  return (
    <div className="App">
      {/* Navbar */}
      <Navbar />
      {/* Header */}
      <Header />
      {/* Item */}
      <div className="container">
        <div className="row ">
          <div className="col-3 ">
            <Item />
          </div>
          <div className="col-3 ">
            <Item />
          </div>
          <div className="col-3 ">
            <Item />
          </div>
          <div className="col-3 ">
            <Item />
          </div>
        </div>
      </div>
      {/* Footer */}
      <Footer />
    </div>
  );
}

export default App;
